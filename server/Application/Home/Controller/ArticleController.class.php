<?php

namespace Home\Controller;

class ArticleController extends CommonController {

    public function index() {
        $map = array();
        $title = trim(I('title'));
        if ($title)
            $map['title'] = array('like', "%{$title}%");
        $this->_list(array('source' => CONTROLLER_NAME, 'map' => $map, 'order' => 'sort asc'));
    }

    public function add() {
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $this->assign('nav_tree', $nav_tree);
        session('uploaded', null);
        $this->display();
    }

    //上传封面
    public function coverUpload() {
        parent::ajaxUpload(
            array(
                'model'=>'Picture',
                'field'=>'cover',
            )
        );
    }

    public function edit($id) {
        $_GET['model'] = CONTROLLER_NAME;
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $this->assign('nav_tree', $nav_tree);
        $this->_edit();
    }

    public function detail() {
        vendor('markdown');
        $chapter_id = I('cid');
        $chapterModel = D('Chapter');
        $chapter = $chapterModel->find($chapter_id);
        $chapter || $this->error('章节不存在！');
        $book = D(CONTROLLER_NAME)->find($chapter['book_id']);
        $book || $this->error('书籍不存在');
        if (IS_AJAX) {
            $this->ajaxReturn($chapter);
        } else {
            //获取目录列表
            $root_id = $chapterModel->getRoot($book['id']);
            $tree = $chapterModel->getDirectoryList($root_id);
            $tree = list_to_tree($tree, 'id', 'pid', '_', $root_id);
            $this->assign('chapter', $chapter);
            $this->assign('book', $book);
            $this->assign('tree', $tree);
            $this->display();
        }
    }

    public function editbleGetCate() {
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $list = array();
        $list[] = array('value' => '', 'text' => '请选择');
        foreach ($nav_tree as $key => $value) {
            $list[] = array('value' => $value['id'], 'text' => $value['title_show']);
        }
        exit(json_encode($list));
    }

        //editble ajax更新方法
    public function ajaxUpdate() {
        $_POST = array(
            'id' => I('pk'),
            I('name') => I('value')
        );
        if(M('Article')->save($_POST) !== false)
            $this->success('');
        else
            $this->error('');
    }

}

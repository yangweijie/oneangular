<?php

namespace Home\Controller;

use Think\Controller;
use Think\Model as MODEL;

class CateController extends CommonController {

    //初始化方法
    protected function _initialize() {

        parent:: _initialize();
    }

    public function index() {
        $pid = (int) I('pid');
        $title = trim(I('title'));
        if ($pid)
            $map['pid'] = $pid;
        else if (!$title)
            $map['pid'] = 0;
        if ($title)
            $map['title'] = array('like', "%{$title}%");

        $current_parent = D('Cate')->field('pid,title')->find($pid);
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $this->assign('nav_tree', $nav_tree);
        $this->assign('current_parent', $current_parent);
        $this->_list(array('source' => CONTROLLER_NAME, 'map' => $map, 'order' => '`sort` asc'));
    }

    public function add() {
        $nav_tree = D('Cate')->where($map)->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $this->assign('nav_tree', $nav_tree);
        $this->display();
    }

    public function edit($id) {
        $_GET['model'] = 'Cate';
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $this->assign('nav_tree', $nav_tree);
        $this->_edit();
    }

    public function editbleGetNav() {
        $nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
        $list = array();
        $list[] = array('value' => '', 'text' => '请选择');
        foreach ($nav_tree as $key => $value) {
            $list[] = array('value' => $value['id'], 'text' => $value['title_show']);
        }
        exit(json_encode($list));
    }

}

<?php

namespace Home\Controller;

use Think\Controller;
use Think\Model as MODEL;

/**
 *
 * @authors yangweijie (yangweijiester@gmail.com)
 * @date    2013-10-6 09:30:32
 * @version $Id$
 */
class UserController extends CommonController {

    public function index() {
        $this->_list(array('source' => CONTROLLER_NAME));
    }

    public function add() {
        $this->display();
    }

}

<?php

#公共函数库
        const ONETHINK_ADDON_PATH = './Addons/';

/**
 * 数组转字符串
 */
function arr2str($str, $sep = ',') {
    return implode($sep, $str);
}

/**
 * 字符串转数组
 */
function str2arr($str, $sep = ',') {
    return explode($sep, $str);
}

//文件名转成文件系统对应的编码
function file_iconv($name) {
    return iconv('UTF-8', C('FILE_SYSTEM_ENCODE'), $name);
}

function admin_log($content, $table) {

}

function url($link = '', $param = '', $default = '') {
    return $default ? $default : U($link, $param);
}

/**
 * 把返回的数据集转换成Tree
 * @access public
 * @param array $list 要转换的数据集
 * @param string $pid parent标记字段
 * @param string $level level标记字段
 * @return array
 */
function list_to_tree($list, $pk = 'id', $pid = 'pid', $child = '_child', $root = 0) {
    // 创建Tree
    $tree = array();
    if (is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] = & $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] = & $list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent = & $refer[$parentId];
                    $parent[$child][] = & $list[$key];
                }
            }
        }
    }
    return $tree;
}

/**
 * 快速文件数据读取和保存 针对简单类型数据 字符串、数组
 * @param string $name 缓存名称
 * @param mixed $value 缓存值
 * @param string $path 缓存路径
 * @return mixed
 */
function d_f($name, $value, $path = DATA_PATH) {
    static $_cache = array();
    $filename = $path . $name . '.php';
    if ('' !== $value) {
        if (is_null($value)) {
            // 删除缓存
            // return false !== strpos($name,'*')?array_map("unlink", glob($filename)):unlink($filename);
        } else {
            // 缓存数据
            $dir = dirname($filename);
            // 目录不存在则创建
            if (!is_dir($dir))
                mkdir($dir, 0755, true);
            $_cache[$name] = $value;
            $content = strip_whitespace("<?php\treturn " . var_export($value, true) . ";?>") . PHP_EOL;
            return file_put_contents($filename, $content, FILE_APPEND);
        }
    }
    if (isset($_cache[$name]))
        return $_cache[$name];
    // 获取缓存数据
    if (is_file($filename)) {
        $value = include $filename;
        $_cache[$name] = $value;
    } else {
        $value = false;
    }
    return $value;
}

//下载文件
function download_file($file, $o_name = '') {
    if (is_file($file)) {
        $length = filesize($file);
        $type = mime_content_type($file);
        $showname = ltrim(strrchr($file, '/'), '/');
        if ($o_name)
            $showname = $o_name;
        header("Content-Description: File Transfer");
        header('Content-type: ' . $type);
        header('Content-Length:' . $length);
        if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
            header('Content-Disposition: attachment; filename="' . rawurlencode($showname) . '"');
        } else {
            header('Content-Disposition: attachment; filename="' . $showname . '"');
        }
        readfile($file);
        exit;
    } else {
        exit('文件不存在');
    }
}

/**
 * Description 截取指定长度的字符串 微博使用 汉字或全角字符占1个长度, 英文字符占0.5个长度
 * @param string $str
 * @param int $len = 140 截取长度
 * @param string $ext = '' 添加的后缀
 * @return string $output
 */
function wb_substr($str, $len = 140, $dots = 1, $ext = '') {
    $str = htmlspecialchars_decode(strip_tags(htmlspecialchars($str)));
    $strlenth = 0;
    $output = '';
    preg_match_all("/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/", $str, $match);
    foreach ($match[0] as $v) {
        preg_match("/[\xe0-\xef][\x80-\xbf]{2}/", $v, $matchs);
        if (!empty($matchs[0])) {
            $strlenth += 1;
        } elseif (is_numeric($v)) {
            $strlenth += 0.545;
        } else {
            $strlenth += 0.475;
        }
        if ($strlenth > $len) {
            $output .= $ext;
            break;
        }
        $output .= $v;
    }
    if ($strlenth > $len && $dots)
        $output.='...';
    return $output;
}

function highlight_map($str, $keyword) {
    return str_replace($keyword, "<em class='keywords'>{$keyword}</em>", $str);
}

//删除文件前处理文件名
function del_file($file) {
    $file = file_iconv($file);
    @unlink($file);
}

function status_text($model, $key) {
    switch ($model) {
        case 'Nav':
            $text = array('无效', '有效');
            break;
    }
    return $text[$key];
}

/**
 * 用户登录session数据签名
 * @param  array  $user 用户登录后保存的session数据
 * @return string       签名
 */
function user_auth_sign($user) {
    ksort($user); //排序
    $code = http_build_query($user); //url编码并生成query字符串
    $sign = sha1($code); //生成签名
    return $sign;
}

//是否超级管理员
function is_admin() {
    $login_name = get_admin_name();
    $createBy = D('Admin')->where("login_name='{$login_name}'")->getField('creator');
    return ($createBy === '0') ? 1 : 0;
}

/**
 * 获取链接信息
 * @param int $link_id
 * @param string $field
 * @return 完整的链接信息或者某一字段
 * @author huajie <banhuajie@163.com>
 */
function get_link($link_id = null, $field = 'url'){
    $link = '';
    if(empty($link_id)){
        return $link;
    }
    $link = M('Url')->getById($link_id);
    if(empty($field)){
        return $link;
    }else{
        return $link[$field];
    }
}


/**
 * 获取文档封面图片
 * @param int $cover_id
 * @param string $field
 * @return 完整的数据  或者  指定的$field字段值
 * @author huajie <banhuajie@163.com>
 */
function get_cover($cover_id, $field = null){
    if(empty($cover_id)){
        return false;
    }
    $picture = M('Picture')->where(array('status'=>1))->getById($cover_id);
    if($field == 'path'){
        if(!empty($picture['url'])){
            $picture['path'] = $picture['url'];
        }else{
            $picture['path'] = __ROOT__.$picture['path'];
        }
    }
    return empty($field) ? $picture : $picture[$field];
}

/**
 * 获取当前登录的用户名
 * @return string 用户名
 */
function get_admin_name() {
    $user = session(C('USER_AUTH_KEY'));
    return $user['admin_name'];
}

//判断用户是否登录
function is_login() {
    $user = session(C('USER_AUTH_KEY'));
    if (empty($user)) {
        return 0;
    } else {
        return session(C('USER_AUTH_SIGN_KEY')) == user_auth_sign($user) ? $user['admin_id'] : 0;
    }
}

function show_tree($tree, $template) {
    $view = new View();
    $view->assign('tree', $tree);
    return $view->fetch($template);
}

/**
 * 对查询结果集进行排序
 * @access public
 * @param array $list 查询结果
 * @param string $field 排序的字段名
 * @param array $sortby 排序类型
 * asc正向排序 desc逆向排序 nat自然排序
 * @return array
 */
function list_sort_by($list, $field, $sortby = 'asc') {
    if (is_array($list)) {
        $refer = $resultSet = array();
        foreach ($list as $i => $data)
            $refer[$i] = &$data[$field];
        switch ($sortby) {
            case 'asc': // 正向排序
                asort($refer);
                break;
            case 'desc':// 逆向排序
                arsort($refer);
                break;
            case 'nat': // 自然排序
                natcasesort($refer);
                break;
        }
        foreach ($refer as $key => $val)
            $resultSet[] = &$list[$key];
        return $resultSet;
    }
    return false;
}

/**
 * 处理插件钩子
 * @param string $hook   钩子名称
 * @param mixed $params 传入参数
 * @return mixed
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function hooks($hook, $params = array()) {
    $hooks = S('hooks');
    if (!$hooks) {
        $hooks = D('Hooks')->getField('name,addons');
        foreach ($hooks as $key => $value) {
            $hooks[$key] = explode(',', $value);
        }
        S('hooks', $hooks);
    }
    $addons = $hooks[$hook];
    if (!empty($addons)) {
        if (APP_DEBUG) {
            G($hook . 'Start');
            trace('[ ' . $hook . ' ] --START--', '', 'INFO');
        }
        // 执行插件
        foreach ($addons as $key => $name) {
            if ($name) {
                $addons_class = addons($name);
                if ($addons_class) {
                    $config = $addons_class->getConfig();
                    if (method_exists($addons_class, $hook) && $config['status'] == 1)
                        $addons_class->$hook($params);
                } else {
                    trace("插件 {$name} 入口文件不存在", 'ADDONS', 'ERR');
                }
            }
        }
        if (APP_DEBUG) { // 记录钩子的执行日志
            trace('[ ' . $hook . ' ] --END-- [ RunTime:' . G($hook . 'Start', $hook . 'End', 6) . 's ]', '', 'INFO');
        }
    } else { // 未注册任何钩子 返回false
        return false;
    }
}

/**
 * select返回的数组进行整数映射转换
 *
 * @param array $map  映射关系二维数组  array(
 *                                          '字段名1'=>array(映射关系数组),
 *                                          '字段名2'=>array(映射关系数组),
 *                                           ......
 *                                       )
 * @author 朱亚杰 <zhuyajie@topthink.net>
 * @return array
 *
 *  array(
 *      array('id'=>1,'title'=>'标题','status'=>'1','status_text'=>'正常')
 *      ....
 *  )
 *
 */
function int_to_string(&$data, $map = array('status' => array(1 => '正常', -1 => '删除', 0 => '禁用', 2 => '未审核', 3 => '草稿'))) {
    if ($data === false || $data === null) {
        return $data;
    }
    $data = (array) $data;
    foreach ($data as $key => $row) {
        foreach ($map as $col => $pair) {
            if (isset($row[$col]) && isset($pair[$row[$col]])) {
                $data[$key][$col . '_text'] = $pair[$row[$col]];
            }
        }
    }
    return $data;
}

/**
 * 处理插件钩子
 * @param string $hook   钩子名称
 * @param mixed $params 传入参数
 * @return void
 */
function hook($hook, $params = array()) {
    \Think\Hook::listen($hook, $params);
}

/**
 * 获取插件类的类名
 * @param strng $name 插件名
 */
function get_addon_class($name) {
    $class = "Addons\\{$name}\\{$name}Addon";
    return $class;
}

/**
 * 获取插件类的配置文件数组
 * @param string $name 插件名
 */
function get_addon_config($name) {
    $class = get_addon_class($name);
    if (class_exists($class)) {
        $addon = new $class();
        return $addon->getConfig();
    } else {
        return array();
    }
}

/**
 * 插件显示内容里生成访问插件的url
 * @param string $url url
 * @param array $param 参数
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function addons_url($url, $param = array()) {
    $url = parse_url($url);
    $case = C('URL_CASE_INSENSITIVE');
    $addons = $case ? parse_name($url['scheme']) : $url['scheme'];
    $controller = $case ? parse_name($url['host']) : $url['host'];
    $action = trim($case ? strtolower($url['path']) : $url['path'], '/');

    /* 解析URL带的参数 */
    if (isset($url['query'])) {
        parse_str($url['query'], $query);
        $param = array_merge($query, $param);
    }

    /* 基础参数 */
    $params = array(
        '_addons' => $addons,
        '_controller' => $controller,
        '_action' => $action,
    );
    $params = array_merge($params, $param); //添加额外参数

    return U('Addons/execute', $params);
}

/**
 * 获取配置的类型
 * @param string $type 配置类型
 * @return string
 */
function get_config_type($type=0){
    $list = C('CONFIG_TYPE_LIST');
    return $list[$type];
}

/**
 * 获取配置的分组
 * @param string $group 配置分组
 * @return string
 */
function get_config_group($group=0){
    $list = C('CONFIG_GROUP_LIST');
    return $group?$list[$group]:'';
}

// 分析枚举类型配置值 格式 a:名称1,b:名称2
function parse_config_attr($string) {
    $array = preg_split('/[,;\r\n]+/', trim($string, ",;\r\n"));
    if(strpos($string,':')){
        $value  =   array();
        foreach ($array as $val) {
            list($k, $v) = explode(':', $val);
            $value[$k]   = $v;
        }
    }else{
        $value  =   $array;
    }
    return $value;
}

/**
 * 调用系统的API接口方法（静态方法）
 * api('User/getName','id=5'); 调用公共模块的User接口的getName方法
 * api('Admin/User/getName','id=5');  调用Admin模块的User接口
 * @param  string  $name 格式 [模块名]/接口名/方法名
 * @param  array|string  $vars 参数
 */
function api($name, $vars = array()) {
    $array = explode('/', $name);
    $method = array_pop($array);
    $classname = array_pop($array);
    $module = $array ? array_pop($array) : 'Common';
    $callback = $module . '\\Api\\' . $classname . 'Api::' . $method;
    if (is_string($vars)) {
        parse_str($vars, $vars);
    }
    return call_user_func_array($callback, $vars);
}

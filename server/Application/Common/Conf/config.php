<?php

return array(
    'AUTOLOAD_NAMESPACE' => array('Addons' => ONETHINK_ADDON_PATH), //扩展模块列表
    //'配置项'=>'配置值'
    'DEFAULT_MODULE' => 'Home',
    'MODULE_DENY_LIST' => array('Common'),
    'MODULE_ALLOW_LIST' => array('Home'),
    'URL_MODEL' => 2,
    /* 数据库配置 */
    'DB_TYPE' => 'mysqli', // 数据库类型
    'DB_HOST' => '127.0.0.1', // 服务器地址
    'DB_NAME' => 'oa', // 数据库名
    'DB_USER' => 'root', // 用户名
    'DB_PWD' => '', // 密码
    'DB_PORT' => '3306', // 端口
    'DB_PREFIX' => 'onethink_', // 数据库表前缀
    'FILE_SYSTEM_ENCODE' => IS_WIN ? 'GBK' : 'UTF-8',
    'SHOW_PAGE_TRACE' => 1,
    /* SESSION配置 */
    'SESSION_PREFIX' => 'admin', //session前缀
    'VAR_SESSION_ID' => 'session_id',
    'FILE_SYSTEM_ENCODE' => IS_WIN ? 'GBK' : 'UTF-8',
    /* cookie记住我时间长度 */
    'REMEMBER_ME_TIME' => 86400 * 365, //365天 86400*365
    /* 项目其他配置 */
    'USER_AUTH_KEY' => 'admin_user',
    'USER_AUTH_SIGN_KEY' => 'admin_user_sign',
    'HOOKS_TYPE' => array(
        1 => '视图',
        2 => '控制器'
    ),
    /* 模板相关配置 */
    'TMPL_PARSE_STRING' => array(
        '__STATIC__' => __ROOT__ . '/Public/static',
        '__ADDONS__' => __ROOT__ . '/Public/Addons',
        '__IMG__' => __ROOT__ . '/Public/images',
        '__CSS__' => __ROOT__ . '/Public/css',
        '__JS__' => __ROOT__ . '/Public/js',
    ),
    'URL_HTML_SUFFIX' => '.html',
);
